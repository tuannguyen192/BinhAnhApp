import { AppRegistry } from 'react-native';
// import App from './App';
import { createStackNavigator } from 'react-navigation';
import {
Home,
DetailsScreen,
DetailsScreen2
} from './App';

const App = createStackNavigator({
    Home: {
      screen: Home
    },
    DetailsScreen: {
        screen: DetailsScreen
      },
    DetailsScreen2: {
        screen: DetailsScreen2
    },
  })
AppRegistry.registerComponent('BinhAnhApp25', () => App);
