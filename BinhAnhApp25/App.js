/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import { Button, View, Text } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import store from './store';
import {observer} from 'mobx-react';

@observer
export class Home extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Screen1</Text>
        <Text>{JSON.stringify(store.todos)}</Text>
        <Button
          title="Go1"
          onPress={() => this.props.navigation.navigate('DetailsScreen')}
        />
      </View>
    );
  }
}

@observer
export class DetailsScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Screen2</Text>
        <Text>{JSON.stringify(store.todos)}</Text>
        <Button
          title="go2"
          onPress={() => this.props.navigation.navigate('DetailsScreen2')}
        />
        <Button
          title="BACCKKKK"
          onPress={() => 
            {
              // this.props.navigation.navigate('Home');
              this.props.navigation.goBack();              
          }}/>
      </View>
    );
  }
}
@observer
export class DetailsScreen2 extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Screen3</Text>
        <Text>{JSON.stringify(store.todos)}</Text>
        <Button
          title="Go3"
          onPress={() => 
            {
              // this.props.navigation.navigate('Home');
              this.props.navigation.goBack();
              store.addNewTodo({
                id: 1,
                todoName: "Develop GMSMapView in IOS",
                description: "Write a MapView and add to a new ViewController"
              });
          }}
        />
      </View>
    );
  }
}
