import React,{Component} from 'react';
import {FlatList, Text, View, SafeAreaView, Image, StyleSheet} from 'react-native';

  
export default class HistoricalComponent extends Component{
    static navigationOptions = ({ navigation }) => {
        const { params } = navigation.state;        
        return {
            title: `Device's Detail`,        
            headerStyle: {
                backgroundColor: 'darkred',
            },
            headerTintColor: 'white',
        }
    }
    constructor(props){
        super(props);
    }
    _convertDatetimeToString = (dateTime) => {
        return new Date(dateTime).toLocaleString();        
    }
    _renderItem = ({item, index}) =>{        
        let {params} = this.props.navigation.state;
        let backgroundColor = this.props.index % 2 == 0 ? 'darksalmon' : 'darkseagreen';
        return(
            <View style={[styles.itemDetail, {backgroundColor}]}>                
                <Text style={styles.dateText}>Date: {this._convertDatetimeToString(item.dateTime)}</Text>
                <Text style={styles.dateText}>Temperatures: {item.degrees}</Text>               
            </View>
        );
    }

    render(){
        let { params } = this.props.navigation.state;
        return(
            <SafeAreaView style={styles.container}>
                <View>
                    <Image
                        source={{uri: params.deviceURL}}
                        style = {{width: '100%', height: 150}}                    
                    />
                    <View style={{ backgroundColor: 'darksalmon', paddingVertical: 10}}>
                        <Text style={{marginHorizontal: 10, fontWeight:'bold'}}>{params.deviceName}</Text>
                        <Text style={{marginHorizontal: 10 }} numberOfLines={2}>{params.description}</Text>
                    </View>
                </View>
            {
                <FlatList                     
                    data={params.histories}
                    renderItem={this._renderItem}
                    keyExtractor={(item, index) => `${index}`}                    
                    style={{ backgroundColor: 'darkseagreen'}}
                />
            }
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    itemDetail: {
        flex: 1,
        flexDirection: 'column',
        padding: 10,
        borderColor: '#000',
        borderBottomWidth: 0.6
    },
    dateText: {
        fontSize: 14
    }

})