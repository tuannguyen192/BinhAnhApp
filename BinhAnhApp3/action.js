import store from './store';

export function addDivice(device){
    store.addNewDevice(device);
}

export function deleteDivice(id){
    store.deleteDevice(id)
}

export function updateDevice(id, device){
    store.updateDevice(id, device)
}