import {observable, autorun, action} from 'mobx';

class Store{   
    @observable devices = [];

    @action addNewDevice(device){
        var time = new Date();
        this.devices = this.devices.concat({
            id: time.getTime(),
            deviceName: device.deviceName,
            description:device.description,
            deviceURL: device.deviceURL,
        });
    }
    @action deleteDevice(id){
        // let existDevices = []
        // this.devices.map((device) =>{
        //     if(device.id !== id){
        //         existDevices.push(device)
        //     }
        // });
        // this.devices = existDevices;
        this.devices = this.devices.filter(divice =>{
            return(divice.id !== id)
        })
    }
    @action updateDevice(id, updateDevice){
        //console.log(id);
        let updatedDevices = [];
        this.devices.map(device=>{
            if(device.id === id){
                updatedDevices.push(updateDevice)
            } else{
                updatedDevices.push(device)
            }
        })
        this.devices = updatedDevices;
    }
}

const store = new Store();
export default store;