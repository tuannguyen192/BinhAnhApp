import { AppRegistry } from 'react-native';
import {StackNavigator} from 'react-navigation'
import DeviceComponent from './components/DeviceComponent';
import React,{Component} from 'react';
import { YellowBox } from 'react-native';
import store from './store';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

class App extends Component{
    render(){
        return(
            <DeviceComponent screenProps={{ store }}/>
        )
    }
}

AppRegistry.registerComponent('BinhAnh', () => App);
