import React,{Component} from 'react';
import {Alert, FlatList, Text, View, SafeAreaView, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {Icon, Fab, Button} from 'native-base';
import { observer } from 'mobx-react/native';
import ModalComponent from './ModalComponent';
import Swipeout from 'react-native-swipeout';
import {deleteDivice} from '../action';


@observer
export default class DeviceComponent extends Component{
    static navigationOptions = {
        headerTitle: "Home",       
        headerTitleStyle: {
            textAlign: 'center', 
            alignSelf: 'center'
        },        
    };
    
    constructor(props){
        super(props);
    }
    _showModel = ()=>{
        this.refs.modalComponent.showModal();
    }
    _renderItem = ({ item, index }) =>{ 
        // console.log(item.deviceName)
        let backgroundColor = index % 2 == 0 ? 'darksalmon' : 'darkseagreen';
        const swipeoutSetting = {
            autoClose: true,
            // onClose: (secId,rowId, direction)=>{

            // },
            // onOpen: (secId,rowId, direction)=>{

            // },
            right: [{
                onPress:()=>{
                    Alert.alert(
                        'Delete',
                        `Are you sure to delete ${item.deviceName}`,
                        [
                            {
                                text: 'Ok',
                                onPress: ()=>{
                                    deleteDivice(item.id)
                                }
                            },
                            {
                                text: 'Cancel',
                                onPress: ()=>{
                                    
                                }
                            }
                        ]

                    )
                },
                text:'Delete', type: 'delete'
            },
            {
                onPress:()=>{
                    this.refs.modalComponent.isUpdate(item);
                },
                text:'Update', type: 'update'
            },
        ],
            rowId: this.props.index,
            sectionId: 1,
        };     
        return(
            <Swipeout {...swipeoutSetting}>
                <View style={[styles.listItem,{backgroundColor}]} >
                    <View style={{width: '15%', justifyContent: 'center'}}>
                        <Image source={{uri: item.deviceURL}}
                        style = {styles.deviceImage}                    
                        />
                    </View>
                    <View style={{width: '85%',}} >
                        <Text style={styles.textItem}>{item.deviceName}</Text>
                        <Text numberOfLines={2} style={styles.descriptionItem}>
                            {item.description}
                        </Text>
                    </View>
                </View>
            </Swipeout>
        );
    };

    render(){
        const{devices} = this.props.screenProps.store;
        //console.log(JSON.stringify(devices));
        return(
            <SafeAreaView style = {styles.container}>
                <FlatList
                    data = {devices}
                    keyExtractor={(item, index) => `${index}`}
                    renderItem={this._renderItem}
                    style={{ backgroundColor: 'darkseagreen' }}
                />
                <Fab
                    position="bottomRight"
                    style={{ backgroundColor: '#5067FF', marginBottom: 20 }}
                    onPress = {()=>{                        
                        this._showModel();
                    }}
                >
                    <Icon style={{color: 'white'}} name= "ios-add-circle-outline"/>
                </Fab>
                <ModalComponent
                    ref="modalComponent"
                    modalComponent = {this.modalComponent}
                ></ModalComponent>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,        
    },
    listItem:{
        flex: 1,      
        backgroundColor: 'white',
        flexDirection: 'row',
        paddingHorizontal: 10,
    }, 
    deviceImage:{
        width: 40,
        height: 40,
        borderRadius: 20,
        resizeMode: 'cover',
        // resizeMode: Image.resizeMode.contain,
    },
    textItem:{
        paddingTop: 10,
        fontSize: 13,
        color: '#000000',
        fontWeight: 'bold',
        marginBottom: 5,        
    },
    descriptionItem:{
        fontSize: 13,        
        paddingBottom: 10
    },    
});