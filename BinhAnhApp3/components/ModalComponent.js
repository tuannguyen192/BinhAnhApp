import React ,{Component} from 'react';
import {addDivice, updateDevice} from '../action';
import {
    StyleSheet, View, Text, TouchableOpacity,
    Platform, Image, TextInput, KeyboardAvoidingView
} from "react-native";
import Modal from 'react-native-modal';

let oldDeviceName = '';
export default class ModalComponent extends Component{
    constructor(props){
        super(props);
        this.state = ({
            deviceName: '',
            description: '',
            imageUrl: 'https://is2-ssl.mzstatic.com/image/thumb/Purple118/v4/f7/fd/36/f7fd36c0-b14e-b762-d51a-b1158ac8ce88/pr_source.png/300x0w.jpg',
            isVisible: false,
            idDevice: '',
        })
    }

    showModal=()=>{
        this.setState({isVisible:true})
    }
    hideModal=()=>{
        this.setState({isVisible: false})
    }
    isUpdate = (deviceUpdate)=>{
        //console.log(deviceUpdate.id);
        oldDeviceName = deviceUpdate.deviceName;
        this.setState({isVisible: true,idDevice: deviceUpdate.id,deviceName: deviceUpdate.deviceName, description: deviceUpdate.description})
        // console.log(this.state.deviceName)
    }

    render(){
        return(
            <Modal
                ref={'myModal'}
                isVisible =  {this.state.isVisible}
                avoidKeyboard = {true}
                onBackdropPress={this.hideModal}
                style = {{alignItems: 'center'}}
            >
                <View style = {styles.modalStyle}>
                    <Text style={{
                            fontSize: 16,
                            fontWeight: 'bold',
                            textAlign: 'center',
                            marginTop: 40
                        }}>{this.state.idDevice === ''? 'Add new device': `Update ${oldDeviceName}`}                    </Text>
                    <View style = {styles.blockViewInput}>
                        <TextInput
                        style = {styles.textInput}
                        placeholder= 'Device name'
                        autoCorrect={false}
                        onChangeText = {text =>{
                            this.setState({deviceName: text})
                        }}
                        value = {this.state.deviceName}
                        />
                        <TextInput
                            style = {styles.textInput}
                            placeholder= 'Device description'
                            autoCorrect={false}
                            onChangeText = {text =>{
                                this.setState({description: text})
                            }}
                            value = {this.state.description}
                        />
                    </View>
                    <View style = {styles.blockViewButton}>
                        <TouchableOpacity 
                            style={styles.buttonContainer}
                            onPress={()=>{
                                if(this.state.description.length == 0 || this.state.deviceName==0){
                                    alert('you need to enter name and descrition of device')
                                    return;
                                }
                                if(this.state.idDevice !== ''){
                                    const id = this.state.idDevice;
                                    const device = {
                                        deviceURL: this.state.imageUrl,
                                        deviceName: this.state.deviceName,
                                        description: this.state.description,
                                    }
                                    updateDevice(id, device)
                                    this.setState({isVisible: false, deviceName: '', description: '', idDevice: ''})
                                } else{
                                    const device = {
                                        deviceName: this.state.deviceName,
                                        description: this.state.description,
                                        deviceURL: this.state.imageUrl
                                    }
                                    addDivice(device)
                                    this.setState({isVisible: false, deviceName: '', description: ''})
                                }
                            }}
                        >   
                            <Text style={styles.textButton}>Ok</Text> 
                        </TouchableOpacity>
                        <TouchableOpacity  
                            style={styles.buttonContainer}
                            onPress = {()=>{
                                this.setState({isVisible: false, deviceName: '', description: ''})
                            }}
                        >
                            <Text style={styles.textButton}>Cancel</Text> 
                        </TouchableOpacity>
                    </View>
                    </View>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    modalStyle : {
        width: 300,
        height: 400,
        justifyContent: 'center',
        backgroundColor: 'white',
    },
    textInput: {
        width: 150, 
        paddingVertical: 10,
        marginBottom: 20,
        borderColor: 'rgb(100,121,133)',
        borderWidth: 1,
        paddingLeft: 10
    },
    buttonContainer:{
        padding: 8,
        height: 40,
        borderRadius: 6,
        backgroundColor: 'mediumseagreen',
        width: 100,
        marginHorizontal: 10
    },
    textButton:{
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20
    }, 
    blockViewButton:{
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    blockViewInput:{
        flex: 1, 
        flexDirection: 'column', 
        alignItems: 'center', 
        marginTop: 50

    }
})