import LocalizedStrings from 'react-native-localization';
import en from './en';
import de from './de';

let languageManager = new LocalizedStrings({en, de});
export default languageManager;