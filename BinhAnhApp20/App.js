/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 * Steps
 * react-native install react-native-languages
 * react-native install react-i18next
 * react-native install i18next
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import languageManager from './languages/languageManager';

export default class App extends Component {  
  render() {    
    return (
      <View style={styles.container}>
        <Text style={styles.text}>
    
          {languageManager["I have a dream"]}     
        </Text>        
        <Text style={styles.textWithParam}>
          {languageManager.formatString(languageManager["I like {0} apples and {1} bananas"], '2', '3')}
        </Text>        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,    
    justifyContent: 'center',
    alignItems: 'center',    
  },  
  text: {
    fontWeight: 'bold',
    fontSize: 25
  },
  textWithParam: {
    fontWeight: 'bold',
    fontSize: 25,
    color: 'darkgreen'
  }
});