package com.binhanhapp17;


import android.widget.Toast;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;

import java.util.Map;
import java.util.HashMap;

import javax.annotation.Nullable;

public class ObjectiveCClass extends ReactContextBaseJavaModule {
    //constants
    private static final String DURATION_SHORT_KEY = "SHORT";
    private static final String DURATION_LONG_KEY = "LONG";

    public ObjectiveCClass(ReactApplicationContext reactContext) {
        super(reactContext);
    }
    @Override
    public String getName() {
        return "ObjectiveCClass";//name in Javascript
    }

    @Nullable
    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        constants.put(DURATION_SHORT_KEY, Toast.LENGTH_SHORT);
        constants.put(DURATION_LONG_KEY, Toast.LENGTH_LONG);
        return constants;
    }
    @ReactMethod
    public void displayAMessage(String message, int duration) {
        Toast.makeText(getReactApplicationContext(), message, duration).show();
    }
    @ReactMethod
    //Callback
    public void sum2Number(Integer x, Integer y,
                           Callback errorCallback,Callback successCallback) {
        try {
            Integer sum = x + y;
            successCallback.invoke(sum);
        } catch (Exception e) {
            errorCallback.invoke(e.getMessage());
        }
    }
    private static final String ERROR_MESSAGE = "Error when getting user's information";
    @ReactMethod
    //Promise
    public void findEvents7(Promise promise) {
        try {
            WritableMap map = Arguments.createMap();
            map.putString("name", "Nguyen Duc Hoang");
            map.putInt("age", 40);
            promise.resolve(map);
        } catch (Exception e) {
            promise.reject(ERROR_MESSAGE, e);
        }
    }

}
