//
//  ObjectiveCViewManager.m
//  BinhAnhApp17
//
//  Created by Nguyen Duc Hoang on 5/14/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "RNTObjectiveCViewManager.h"
#import <React/RCTViewManager.h>

@implementation RNTObjectiveCViewManager

RCT_EXPORT_MODULE()
- (UIView *)view {
  UILabel *lblName = [[UILabel alloc] init];
  lblName.text = @"Hello. I am an UILabel in Objective C";
//  lblName.frame = CGRectMake(0, 0, 200, 50);
  return lblName;
}

@end
