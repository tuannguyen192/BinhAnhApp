//
//  NSString+RCTConvert_StatusBarAnimation.m
//  BinhAnhApp17
//
//  Created by Nguyen Duc Hoang on 5/10/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "RCTConvert_CompassPoint.h"

@implementation RCTConvert (CompassPoint)

RCT_ENUM_CONVERTER(CompassPoint, (@{ @"north" : @(North),
                                             @"south" : @(South),
                                             @"east" : @(East),
                                             @"west" : @(West)}),
                   South, integerValue)
@end
