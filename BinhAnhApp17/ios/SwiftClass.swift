//
//  SwiftClass.swift
//  BinhAnhApp17
//
//  Created by Nguyen Duc Hoang on 5/10/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

import UIKit
@objc(SwiftClass)
class SwiftClass: NSObject {
  @objc(addEvent11:location:)
  func addEvent11(name: String, location: String) -> Void {
    print("11.You added : \(name) at location: \(location)")
  }
  @objc(addEvent22:location:date:)
  func addEvent22(name: String, location: String, date: NSNumber) -> Void {
    let date = RCTConvert.nsDate(date)
    print("22.You added : \(name) at location: \(location), at date: \(date!)")
  }
  
  @objc(addEvent33:location:date:)
  func addEvent33(name: String, location: String, date: Date) -> Void {
    print("33.You added : \(name) at location: \(location), at date: \(date)")
  }
  @objc(addEvent44:details:)
  func addEvent44(name: String, details: Dictionary<String, Any>) -> Void {
    let location = RCTConvert.nsString(details["location"])
    let date = RCTConvert.nsDate(details["date"])
    print("44.You added : \(name) at location: \(location!), at date: \(date!)")
  }
  @objc(addEvent55:details:)
  func addEvent55(name: String, details: NSArray) -> Void {
    print("55.You added : \(name)")
    for (index, object) in details.enumerated() {
      print("index = \(index), object = \(object)")
    }
  }
  //The return type of bridge methods is always void. React Native bridge is asynchronous, so the only way to pass a result to JavaScript is by using callbacks or emitting events
  @objc(findEvents66:)
  func findEvents66(callback: RCTResponseSenderBlock) -> Void {
    let events = [
      ["name": "Hoang","email":"hoang@gmail.com"],
      ["name": "Alex","email":"alex@gmail.com"],
      ["name": "John","email":"john@gmail.com"]
    ];
    print("66.Using callback")
    callback([NSNull.init(), events])
  }
  //Promise
  @objc(findEvents77:rejecter:)
  func findEvents77(resolve: RCTPromiseResolveBlock, rejecter: RCTPromiseRejectBlock) -> Void {
    let events = [
      ["name": "Hoang","email":"hoang@gmail.com"],
      ["name": "Alex","email":"alex@gmail.com"],
      ["name": "John","email":"john@gmail.com"]
    ];
    print("77.Using Promise")
    if (events.count > 0) {
      resolve([events]);
    } else {
      let error = NSError(domain: "com.BinhAnhApp.RNTuts", code: 120, userInfo: nil)
      rejecter("no_events", "There were no events", error);
    }
  }
  @objc(doHeavyTasks:callback:)
  func doHeavyTasks(name: String, callback: @escaping RCTResponseSenderBlock) -> Void {
    DispatchQueue.global(qos: .utility).async {
      // Asynchronous code running on the low priority queue
      let events = [
        ["name": "Hoang","email":"hoang@gmail.com"],
        ["name": "Alex","email":"alex@gmail.com"],
        ["name": "John","email":"john@gmail.com"]
      ];
      print("doHeavyTasks...")
      callback([NSNull.init(), events])
    }
  }
  @objc
  func constantsToExport() -> Dictionary<String, Any> {
    return [
      "lastDayOfWeek": "Sunday",
      "PI": 3.1416
    ]
  }
}
