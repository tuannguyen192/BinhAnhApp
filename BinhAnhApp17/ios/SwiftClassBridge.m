//
//  SwiftClassBridge.m
//  BinhAnhApp17
//
//  Created by Nguyen Duc Hoang on 5/11/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <React/RCTBridgeModule.h>
@interface RCT_EXTERN_MODULE(SwiftClass, NSObject)

RCT_EXTERN_METHOD(addEvent11:(NSString *)name location:(NSString *)location)
RCT_EXTERN_METHOD(addEvent22:(NSString *)name location:(NSString *)location date:(nonnull NSNumber *)date)
RCT_EXTERN_METHOD(addEvent33:(NSString *)name location:(NSString *)location date:(NSDate *)date)
RCT_EXTERN_METHOD(addEvent44:(NSString *)name details:(NSDictionary *)details)
RCT_EXTERN_METHOD(addEvent55:(NSString *)name details:(NSArray *)details)
RCT_EXTERN_METHOD(findEvents66:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(findEvents77: (RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
RCT_EXTERN_METHOD(doHeavyTasks:(NSString *)param callback:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(constantsToExport)

@end

