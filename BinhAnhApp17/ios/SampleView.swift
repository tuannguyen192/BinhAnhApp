//
//  File.swift
//  BinhAnhApp17
//
//  Created by Nguyen Duc Hoang on 5/9/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

import UIKit
class SampleView: UIView {
  override init(frame: CGRect) {
    super.init(frame: frame)
    let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
    label.text = "This is a label in Swift"
    self.addSubview(label)
  }
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
