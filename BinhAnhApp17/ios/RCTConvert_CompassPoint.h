//
//  NSString+RCTConvert_StatusBarAnimation.h
//  BinhAnhApp17
//
//  Created by Nguyen Duc Hoang on 5/10/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTConvert.h>
typedef NS_ENUM(NSInteger, CompassPoint) {
  North,
  South,
  East,
  West
};
@interface RCTConvert (CompassPoint)

@end

