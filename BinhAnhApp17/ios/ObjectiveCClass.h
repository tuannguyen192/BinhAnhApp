//
//  ObjectiveCClass.h
//  BinhAnhApp17
//
//  Created by Nguyen Duc Hoang on 5/10/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTConvert.h>
#import "RCTConvert_CompassPoint.h"

@interface ObjectiveCClass : NSObject<RCTBridgeModule>

@end
