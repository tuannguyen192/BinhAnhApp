//
//  ObjectiveCClass.m
//  BinhAnhApp17
//
//  Created by Nguyen Duc Hoang on 5/10/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "ObjectiveCClass.h"
@import UIKit;

@implementation ObjectiveCClass
RCT_EXPORT_MODULE();
/*
RCT_EXPORT_METHOD(addEvent1:(NSString *)name atLocation:(NSString *)location) {
    NSLog(@"1.You added : %@ at location: %@", name, location);
}
*/
/*
RCT_EXPORT_METHOD(addEvent2:(NSString *)name location:(NSString *)location date:(nonnull NSNumber *)timestamp) {
  NSDate *date = [RCTConvert NSDate:timestamp];
  NSLog(@"2.You added : %@ at location: %@, at date: %@", name, location, date);
}
 */
/*
RCT_EXPORT_METHOD(addEvent3:(NSString *)name location:(NSString *)location date:(NSDate *)date) {
  NSLog(@"3.You added : %@ at location: %@, at date: %@", name, location, date);
}
*/
/*
RCT_EXPORT_METHOD(addEvent4:(NSString *)name details:(NSDictionary *)details) {
  NSString *location = [RCTConvert NSString:details[@"location"]];
  NSDate *date = [RCTConvert NSDate:details[@"date"]];
  NSLog(@"4.You added : %@ at location: %@, at date: %@", name, location, date);
}
 */
/*
RCT_EXPORT_METHOD(addEvent5:(NSString *)name details:(NSArray *)details) {
  NSLog(@"5.You added : %@ \n", name);
  [details enumerateObjectsUsingBlock:^(id object, NSUInteger idx, BOOL *stop) {
    NSLog(@"object : %@", object);
  }];
}
*/
//The return type of bridge methods is always void. React Native bridge is asynchronous, so the only way to pass a result to JavaScript is by using callbacks or emitting events
/*
RCT_EXPORT_METHOD(findEvents6:(RCTResponseSenderBlock)callback) {
  
  NSArray *events = [NSArray arrayWithObjects:
                     [NSDictionary dictionaryWithObjectsAndKeys:
                      @"Hoang",@"name",
                      @"hoang@gmail.com",@"email", nil],
                     [NSDictionary dictionaryWithObjectsAndKeys:
                      @"Alex",@"name",
                      @"alex@gmail.com",@"email", nil],
                     [NSDictionary dictionaryWithObjectsAndKeys:
                      @"John",@"name",
                      @"john@gmail.com",@"email", nil],
                     nil];
  callback(@[[NSNull null], events]);//only array!
}
 */
//Promise
RCT_REMAP_METHOD(findEvents7,
                 findEventsWithResolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
  NSArray *events = [NSArray arrayWithObjects:
                     [NSDictionary dictionaryWithObjectsAndKeys:
                      @"Hoang",@"name",
                      @"hoang@gmail.com",@"email", nil],
                     [NSDictionary dictionaryWithObjectsAndKeys:
                      @"Alex",@"name",
                      @"alex@gmail.com",@"email", nil],
                     [NSDictionary dictionaryWithObjectsAndKeys:
                      @"John",@"name",
                      @"john@gmail.com",@"email", nil],
                     nil];
  if (events) {
    resolve(events);
  } else {
    NSError *error = [NSError errorWithDomain:@"com.BinhAnhApp.RNTuts"
                                         code:120
                                     userInfo:@{@"Error reason": @"Invalid ABC..."}];
    reject(@"no_events", @"There were no events", error);
  }
}
/*
//Promise
RCT_REMAP_METHOD(findEvents7,
                 findEventsWithResolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
  NSArray *events = [NSArray arrayWithObjects:
                     [NSDictionary dictionaryWithObjectsAndKeys:
                      @"Hoang",@"name",
                      @"hoang@gmail.com",@"email", nil],
                     [NSDictionary dictionaryWithObjectsAndKeys:
                      @"Alex",@"name",
                      @"alex@gmail.com",@"email", nil],
                     [NSDictionary dictionaryWithObjectsAndKeys:
                      @"John",@"name",
                      @"john@gmail.com",@"email", nil],
                     nil];
  if (events) {
    resolve(events);
  } else {
    NSError *error = [NSError errorWithDomain:@"com.BinhAnhApp.RNTuts"
                                         code:120
                                     userInfo:@{@"Error reason": @"Invalid ABC..."}];
    reject(@"no_events", @"There were no events", error);
  }
}
RCT_EXPORT_METHOD(doHeavyTasks:(NSString *)param callback:(RCTResponseSenderBlock)callback) {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    // Call long-running code on background thread
    // You can invoke callback from any thread/queue
    NSLog(@"doHeavyTasks");
    NSArray *events = [NSArray arrayWithObjects:
                       [NSDictionary dictionaryWithObjectsAndKeys:
                        @"Hoang",@"name",
                        @"hoang@gmail.com",@"email", nil],
                       [NSDictionary dictionaryWithObjectsAndKeys:
                        @"Alex",@"name",
                        @"alex@gmail.com",@"email", nil],
                       [NSDictionary dictionaryWithObjectsAndKeys:
                        @"John",@"name",
                        @"john@gmail.com",@"email", nil],
                       nil];
    callback(@[[NSNull null], events]);//only array!
  });
}
*/
/*
- (NSDictionary *)constantsToExport {
  return @{
           @"firstDayOfTheWeek": @"Monday",
           @"PI": [NSNumber numberWithDouble:3.1416],
           };
}
RCT_EXPORT_METHOD(callEnum:(CompassPoint)compassPoint completion:(RCTResponseSenderBlock)callback) {
  NSLog(@"callEnum : %ld", (long)compassPoint);
  callback(@[[NSNull null], @{@"status":@"ok"}]);//only array!
}
*/
 /*



*/
@end
