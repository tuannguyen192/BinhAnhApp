/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
/*
var ObjectiveCClass = NativeModules.ObjectiveCClass;
var SwiftClass = NativeModules.SwiftClass;

import { NativeModules } from 'react-native';
*/
import MyView from './MyView.js';

import { NativeModules } from 'react-native';
import JavaModule from './JavaModule';
// var ObjectiveCClass = NativeModules.ObjectiveCClass;
import ObjectiveCClass from './ObjectiveCClass';
export default class App extends Component {  
render() {  
  // ObjectiveCClass.addEvent1('Learn React Native', 'BinhAnh company');
  //ObjectiveCClass.addEvent2('Learn React Native', 'BinhAnh company', 1521676800 * 1000);
  //ObjectiveCClass.addEvent3('Learn React Native', 'BinhAnh company', new Date());
  /*
  ObjectiveCClass.addEvent4('Learn React Native', {
    location: 'BinhAnh company',
    date: new Date()
  });
  */
 /*
 ObjectiveCClass.addEvent5('Learn React Native',
    [{ name: "Hoang", email: "hoang@gmail.com", }, "Hello world", 120]
  );
  */
 /*
 ObjectiveCClass.findEvents6((error, events) => {
  if (error) {
    console.error(`error = ${error}`);
  } else {
    // this.setState({ events: events });
    console.log(`events = ${JSON.stringify(events)}`);
  }
});
*/
/*
ObjectiveCClass.findEvents7().then(()=>{
  console.log('Call a promise success');
}).catch((error)=>{
  console.log(`Call a promise failed. Error = ${error}`);
});
*/
/*
ObjectiveCClass.doHeavyTasks("I am doing a heavy task", (error, events) => {
  if (error) {
    console.error(`Heavy task. error = ${error}`);
  } else {
    // this.setState({ events: events });
    console.log(`Heavy task. events = ${JSON.stringify(events)}`);
  }
});
*/
  /*
  console.log(`First day of the week = ${ObjectiveCClass.firstDayOfTheWeek}`);
  console.log(`PI = ${ObjectiveCClass.PI}`);  
  */
  /*    
  
  
  ObjectiveCClass.callEnum(2,(error, result)=>{
    if (error) {
      console.error(`Call enum. error = ${error}`);
    } else {
      // this.setState({ events: events });
      console.log(`Call enum. events = ${JSON.stringify(result)}`);
    }
  });
  */
 /*
  SwiftClass.addEvent11('Learn React Native', 'BinhAnh company');
  SwiftClass.addEvent22('Learn React Native', 'BinhAnh company', 1521676800 * 1000);
  SwiftClass.addEvent33('Learn React Native', 'BinhAnh company', new Date());
  SwiftClass.addEvent44('Learn React Native', {
    location: 'BinhAnh company',
    date: new Date()
  });
  SwiftClass.addEvent55('Learn React Native',
    [{ name: "Hoang", email: "hoang@gmail.com", }, "Hello world", 120]
  );

  SwiftClass.findEvents66((error, events) => {
    if (error) {
      console.error(`error = ${JSON.stringify(error)}`);
    } else {
      // this.setState({ events: events });
      console.log(`events = ${JSON.stringify(events)}`);
    }
  });

  SwiftClass.findEvents77().then(() => {
    console.log('Call a promise success');
  }).catch((error) => {
    console.log(`Call a promise failed. Error = ${JSON.stringify(error)}`);
  });
  SwiftClass.doHeavyTasks("I am doing a heavy task", (error, events) => {
    if (error) {
      console.error(`Heavy task. error = ${error}`);
    } else {
      // this.setState({ events: events });
      console.log(`Heavy task. events = ${JSON.stringify(events)}`);
    }
  });
  console.log(`Last day of the week = ${SwiftClass.lastDayOfWeek}`);
  console.log(`PI = ${SwiftClass.PI}`);  
  */

  // JavaModule.displayAMessage("Hello from Java", JavaModule.LONG);
  //Call a "callback function" in Java
  /*
  JavaModule.sum2Number(1, 2, 
    (errorString) => {
      alert(`errorString = ${errorString}`);
    }, 
    (sum) => {
      alert(`sum = ${sum}`);
    }
  );
  */
  //Promise
  /*
  ObjectiveCClass.findEvents7().then(() => {
    alert('Call a promise success');
  }).catch((error) => {
    console.log(`Call a promise failed. Error = ${error}`);
  });
  */
/*
  JavaModule.getAnUser("12345")
  .then((userObject)=>{
    alert(`name = ${userObject.name}, age = ${userObject.age}`)
  }).catch((e1)=>{
    alert(`error1 = ${JSON.stringify(e1)}`)
    }).catch((e2) => {
      alert(`error2 = ${JSON.stringify(e2)}`)
    });
  */
  //async / await
  
  return (
    <View style={styles.container}>
      <Text style={styles.welcome}>Welcome to React Native! </Text>      
      <MyView style={{ backgroundColor: 'green', width: 120, height: 50}}></MyView>
    </View>
  );
}
}

async function getUser() {
  try {
    var { name, age } = await JavaModule.getAnUser("12345");
    alert(`name = ${name}, age = ${age}`)
  } catch (e) {
    alert(`error = ${e}`)
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,    
  },  
});
