/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { FlatList, 
  Dimensions,
  Text, View, SafeAreaView, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { devices } from './devices.js';
import { getMultiSize } from './LayoutManager';

class MyListItem extends Component {

  render() {
    let backgroundColor = this.props.index % 2 == 0 ? 'darksalmon' : 'darkseagreen';
    return (
      <View style={[styles.listItem, { backgroundColor }]}>
        <View style={{ width: '15%', justifyContent: 'center' }}>
          <Image source={{ uri: this.props.item.deviceURL }}
            style={styles.deviceImage}
          />
        </View>
        <View style={{ width: '85%', }} >
          <Text style={styles.textItem}>{this.props.item.deviceName}</Text>
          <Text numberOfLines={2} style={styles.descriptionItem}>{this.props.item.description}</Text>
          <View style={styles.bottomLine}></View>
        </View>
      </View>
    );
  }
}

export default class App extends Component {  
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    console.log("componentDidMount");
    Dimensions.addEventListener("change", (event)=>{
      let {height, width} = Dimensions.get('window');
      console.log(`xoay. w =${width}, h = ${height}`);
    });
}
shouldComponentUpdate(nextProps, nextState, nextContext) {
    console.log("Calling shouldComponentUpdate");
    console.log(`nextProps = ${JSON.stringify(nextProps)}`);        
    console.log(`nextState = ${JSON.stringify(nextState)}`);       
    return true;         
}
componentWillUpdate() {        
    console.log("componentWillUpdate");        
}
componentDidUpdate() {
    console.log("componentDidUpdate");
}
componentWillUnmount() {
    console.log("componentWillUnmount");
}


  _renderItem = ({ item, index }) => (
    <MyListItem
      // {...item}
      item={item}
      index={index}
    />
  );
  render() {
    console.log("renderrrrrrr");
    return (
      <SafeAreaView style={styles.container}>
        <FlatList
          data={devices}
          keyExtractor={(item, index) => `${item.id}`}
          renderItem={this._renderItem}
          style={{ backgroundColor: 'darkseagreen' }}
        />
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listItem: {
    backgroundColor: 'white',
    flexDirection: 'row',
    paddingHorizontal: getMultiSize(10, 20, 25),
  },
  deviceImage: {
    width: getMultiSize(40, 80, 100),
    height: getMultiSize(40, 80, 100),
    borderRadius: getMultiSize(20, 40, 50),
    resizeMode: Image.resizeMode.cover,
    // resizeMode: Image.resizeMode.contain,
  },
  textItem: {
    paddingTop: getMultiSize(10, 20, 25),
    fontSize: getMultiSize(14, 23, 25),
    color: '#000000',
    fontWeight: 'bold',
    marginBottom: getMultiSize(5, 10, 15),
  },
  descriptionItem: {
    fontSize: getMultiSize(14, 22, 24),
    paddingBottom: getMultiSize(10, 20, 25),
  },
  bottomLine: {
    height: 1,
    backgroundColor: 'gray'
  }
});