import React, { Component } from 'react';
import { Dimensions } from 'react-native';
import { Platform } from 'react-native'
import DeviceInfo from 'react-native-device-info';
 
const { height, width } = Dimensions.get('window');
const aspectRatio = height / width;
const currentArea = width * height;
const isTablet = DeviceInfo.isTablet()

export const getMultiSize = (phoneSize, padSize, bigPadSize)=>{
    if (isTablet) {      
        if (currentArea <= 768 * 1024) {
            //normal Pad's size
            return padSize * currentArea / (768 * 1024);//ipad Air
        } else {
            //Big pad
            return bigPadSize * currentArea / (1024 * 1366);//ipad pro 12 inch
        }        
    } else {
        return phoneSize * currentArea / (375 * 812);//iphone X
    }
    
}
