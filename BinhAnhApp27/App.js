/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {  
  StyleSheet,
  Text,
  View
} from 'react-native';
import FCM, { FCMEvent, 
  RemoteNotificationResult, 
  WillPresentNotificationResult, 
  NotificationType 
} from "react-native-fcm";

import { Platform } from 'react-native';

export default class App extends Component{
  componentDidMount() {
    FCM.requestPermissions();

    FCM.getFCMToken().then(token => {
      console.log(`FCM token = ${JSON.stringify(token)}`);
    });

    FCM.getInitialNotification().then(notif => {
      console.log("INITIAL NOTIFICATION", notif)
    });
    
    this.notificationListner = FCM.on("FCMNotificationReceived", async (notif) => {
      alert(`Notification details = ${JSON.stringify(notif)}`);
      
      if (notif.local_notification) {
        return;
      }
      // alert(`You send notification: ${notif.aps.alert}`);//Send string
      if (Platform.OS === 'ios') {
        // alert(`You send notification: ${JSON.stringify(notif.aps.alert)}`);//Send string
        alert(`You send notification with title: 
                ${notif.aps.alert.title}
                body: ${notif.aps.alert.body}
                age: ${notif.age}
                name: ${notif.name}`);//Send object
      } else {
        alert(`You send notification: ${JSON.stringify(notif.fcm.body)}`);//Send string
        alert(`You send notification with title: 
                ${notif.fcm.title}
                body: ${notif.fcm.body}
                age: ${notif.age}
                name: ${notif.name}`);//Send object
      }
      
      this.showLocalNotification(notif);    
      
      if (Platform.OS === 'ios') {
        //optional
        //iOS requires developers to call completionHandler to end notification process. If you do not call it your background remote notifications could be throttled, to read more about it see the above documentation link.
        //This library handles it for you automatically with default behavior (for remote notification, finish with NoData; for WillPresent, finish depend on "show_in_foreground"). However if you want to return different result, follow the following code to override
        //notif._notificationType is available for iOS platfrom
        switch (notif._notificationType) {
          case NotificationType.Remote:
            notif.finish(RemoteNotificationResult.NewData) //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
            break;
          case NotificationType.NotificationResponse:
            notif.finish();
            break;
          case NotificationType.WillPresent:
            notif.finish(WillPresentNotificationResult.All) //other types available: WillPresentNotificationResult.None
            break;
        }
      }      
    });
    this.refreshTokenListener = FCM.on("FCMTokenRefreshed", token => {
      console.log("TOKEN (refreshUnsubscribe)", token);
    });
  }

  showLocalNotification(notif) {
    FCM.presentLocalNotification({
      title: notif.title,
      body: notif.body,
      priority: "high",
      click_action: notif.click_action,
      show_in_foreground: true,
      local: true
    });  
  }
  componentWillUnmount() {
    this.notificationListner.remove();
    this.refreshTokenListener.remove();
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={{fontSize: 25}}>
          React Native Notifications
        </Text>        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },  
});
