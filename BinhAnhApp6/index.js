/*
Steps:
npm install --save express
npm install --save morgan
npm install --save cookie-parser
npm install --save body-parser
npm install --save nodemon
npm install --save babel-preset-es2015 
npm install --save mongoose
npm i -g babel-cli
nodemon ./index.js --exec babel-node
*/

/**
 * Connect DB:
 * Step 1:
 rm -rf /Users/hoangnd/tutorials/NodeJsTutorialES6/database
 mkdir  /Users/hoangnd/tutorials/NodeJsTutorialES6/database
 mongod --port 27017 --dbpath /Users/hoangnd/Documents/code/BinhAnhApp/BinhAnhApp6/database
 Step 2:
 Connect to DB, create database user:
 mongo --port 27017
 use tutorialMongoDB
 db.createUser({
   user: "hoangnd",
   pwd: "hoangnd",
   roles: [ "readWrite", "dbAdmin", "dbOwner" ]
 })
 Step 3:
 Re-start the MongoDB instance with access control.
 mongod --auth --port 27017 --dbpath /Users/hoangnd/Documents/code/BinhAnhApp/BinhAnhApp6/database
 Step 4:
 Connect mongdDB:
 mongo --port 27017 -u "hoangnd" -p "hoangnd" --authenticationDatabase "database1"
 */
import express from 'express';
import path from 'path';
import logger from 'morgan';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import {deviceRouter} from './routes/deviceRouter';

import mongoose from 'mongoose';
let options = {    
    user: 'hoangnd',
    pass: 'hoangnd'
};
// Use native Promises
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/tutorialMongoDB', options).then(
    () => {
        console.log("connect DB successfully");
    },
    err => {
        console.log(`Connection failed. Error: ${err}`);
    }
);
const app = express();
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/device', deviceRouter);
app.listen(3000, () => console.log('Example app listening on port 3000!'));

