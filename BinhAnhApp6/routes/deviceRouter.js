import express from 'express';
import mongoose from 'mongoose';
export const deviceRouter = express.Router();
import { DeviceModel } from '../models/DeviceModel';

// middleware that is specific to this router
// define the home page route
deviceRouter.get('/', function (req, res) {//http://localhost:3000/device
    res.send('Device home');
});
// define the about route
deviceRouter.get('/get_all_devices', (req, res) => {
    //http://localhost:3000/device/get_all_devices
    //res.send('You called get_all_devices');
    DeviceModel.find({}).limit(100).sort({ deviceName: 1 }).select({
        deviceName: 1,
        description: 1,
        deviceURL: 1,
    }).exec((err, devices) => {
        if (err) {
            res.json({
                result: "failed",
                data: [],
                message: `Error is : ${err}`
            });
        } else {
            res.json({
                result: "ok",
                data: devices,
                count: devices.length,
                message: "Query list of devices successfully"
            });
        }
    });
});
//POST method
deviceRouter.post('/add_new_device', (req, res, next) => {
    //http://localhost:3000/device/add_new_device   
    //curl -X POST http://localhost:3000/device/add_new_device -d "@data.txt"
    // res.send('You called get_all_devices'); 
    console.log(JSON.stringify(req.body));
    const criteria = {
        name: new RegExp('^' + req.body.deviceName.trim() + '$', "i")
    };
    DeviceModel.find(criteria).limit(1).exec((err, devices)=>{
        if (err) {
            res.json({
                result: "failed",
                data: {},
                message: `Error is : ${err}`
            });
        } else {
            //Trùng, ko cho insert
            if(devices.length > 0) {
                res.json({
                    result: "failed",
                    data: {},
                    message: `Cannot insert 2 devices with the same name!`
                }); 
                return;
            }
            //Insert
            const newDevice = new DeviceModel({
                deviceName: req.body.deviceName,
                description: req.body.description,
                deviceURL: req.body.deviceURL,
            });
            
            newDevice.save((err, addedDevice) => {
                if (err) {
                    res.json({
                        result: "failed",
                        data: {},
                        messege: `Error in adding new device : ${err}`
                    });
                } else {
                    res.json({
                        result: "ok",
                        data: addedDevice,
                        messege: "Insert new device successfully"
                    });
                }
            });
        }
    });
});
deviceRouter.put('/update_a_device', (req, response) => {
    let conditions = {};//search record with "conditions" to update
    if (mongoose.Types.ObjectId.isValid(req.body.deviceId) == true) {
        conditions._id = mongoose.Types.ObjectId(req.body.deviceId);
    } else {
        response.json({
            result: "failed",
            data: {},
            messege: "You must enter deviceId to update"
        });
        return;
    }
    let newValues = {};
    if (req.body.deviceName && req.body.deviceName.length > 2) {
        newValues.deviceName = req.body.deviceName;
    }
    if (req.body.deviceURL && req.body.deviceURL.length > 2) {
        newValues.deviceURL = req.body.deviceURL;
    }
    const options = {
        new: true, // return the modified document rather than the original.
        multi: true
    }        
    DeviceModel.findOneAndUpdate(conditions, { $set: newValues }, options, (err, updatedDevice) => {
        if (err) {
            response.json({
                result: "failed",
                data: {},
                messege: `Cannot update existing device.Error is : ${err}`
            });
            return;
        } else {
            response.json({
                result: "ok",
                data: updatedDevice,
                messege: "Update device successfully"
            });
        }
    });
});

deviceRouter.delete('/delete_a_device', (req, res) => {
    console.log(req.body)
    let conditions = {};//search record with "conditions" to update
    if (mongoose.Types.ObjectId.isValid(req.body.deviceId) == true) {
        conditions._id = mongoose.Types.ObjectId(req.body.deviceId);
    } else {
        res.json({
            result: "failed",
            data: {},
            messege: "You must enter deviceId to delete"
        });
        return;
    }            
    DeviceModel.findOneAndRemove(conditions, (err, response) => {
        if (err) {
            res.json({
                result: "failed",
                data: {},
                messege: `Cannot delete existing device.Error is : ${err}`
            });
            return;
        } else {
            res.json({
                result: "ok",
                data: response,
                messege: "Delete device successfully"
            });
            return;
        }
    });
});