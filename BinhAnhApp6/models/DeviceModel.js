import mongoose from 'mongoose';
const Schema = mongoose.Schema; 

var DeviceSchema = new Schema({
    deviceName: {
        type: String,
        required: true
    },    
    description: {
        type: String,
        default: ""
    },
    deviceURL: {
        type: String,
        default: "#"
    },
});
// a setter
DeviceSchema.path('deviceName').set((inputString) => {    
    return inputString[0].toUpperCase() + inputString.slice(1);
});
export const DeviceModel = mongoose.model('Device', DeviceSchema);
