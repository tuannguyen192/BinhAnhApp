/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text, AsyncStorage,
  TouchableOpacity,
  View
} from 'react-native';

class MyComponent extends Component {
  funcChild = () => {
    alert("This is a child");
  }
  render(){
    return <TouchableOpacity onPress={()=>{
      if (this.props.onPress) {
        this.props.onPress();
        return;
      }
      this.funcChild();
    }}>
      <Text>Click here</Text>
    </TouchableOpacity>
  }
}
export default class App extends Component{
  funcParent = () => {
    alert("This is a parent");
  }
  constructor(props) {
    super(props);
    this.state = { 
      loggedInUsername: "",
      userObject: null,
      userObjectArray: null
    };  
  }
  async componentDidMount() {    
    
    AsyncStorage.setItem(
      'loggedInUsername', 'hoangnd',
    ).then(() => {
      // alert('Save string to AsyncStorage successfully');
    })
    .catch(error => {
      // alert('error when save string to AsyncStorage');
    });    
    AsyncStorage.setItem(
      'userObject', JSON.stringify({
        name: 'Nguyen Duc Hoang',
        email: 'hoang@gmail.com',
        age: 40
      })).then(() => {
        // alert('Save object to AsyncStorage successfully');
      })
      .catch(error => {
        alert('error when save object to AsyncStorage');
      });   
      AsyncStorage.setItem(
        'userObjectArray', JSON.stringify([{
          name: 'Nguyen Duc Hoang',
          email: 'hoang@gmail.com',
          age: 40
        },{
          name: 'Nguyen Duc Hoang 2',
          email: 'hoang22@gmail.com',
          age: 50
        }
      ])).then(() => {
          // alert('Save object to AsyncStorage successfully');
        })
        .catch(error => {
          alert('error when save object to AsyncStorage');
        });    
      
    AsyncStorage.getItem('loggedInUsername')
      .then(loggedInUsername => {
        this.setState({ loggedInUsername });
      }).catch(error => {
        alert(`error = ${JSON.stringify(error)}`);
      });
    //Get object from AsyncStorage
    AsyncStorage.getItem('userObject')
      .then(aString => {
        this.setState({ userObject: JSON.parse(aString) });
        console.log(`this.userObject.name=${this.userObject.name}`);
      }).catch(error => {
        console.log(`error when get Object = ${JSON.stringify(error)}`);
      }); 
      AsyncStorage.getItem('userObjectArray')
      .then(aString => {
        this.setState({ userObjectArray: JSON.parse(aString) });
        // console.log(`this.userObject.name=${this.userObject.name}`);
      }).catch(error => {
        console.log(`error when get Object Array= ${JSON.stringify(error)}`);
      });   
      AsyncStorage.getAllKeys().then(strings => {
        alert(`strings = ${JSON.stringify(strings)}`);
      }).catch(error => {
        alert(`err = ${error}`);
      })
      /*
    //Get kiểu async/await
    let loggedInUsername = await AsyncStorage.getItem('loggedInUsername');
    this.setState({ loggedInUsername });
    let aString = await AsyncStorage.getItem('userObject');
    this.setState({ userObject: JSON.parse(aString) });
    */
   saveStorage()
  }
  async saveStorage(){
    let loggedInUsername = await AsyncStorage.getItem('loggedInUsername');
    this.setState({ loggedInUsername });
    let aString = await AsyncStorage.getItem('userObject');
    this.setState({ userObject: JSON.parse(aString) });
  }
  
  render() {
    const {userObject} = this.state;                
    
    return (
      <View style={styles.container}>
        <Text
        style={styles.title}>AsyncStorage example</Text>
        {this.state.loggedInUsername && <Text style={ styles.title}>Username = {this.state.loggedInUsername}</Text>}
        {userObject &&
          <Text style={styles.title}>name:{this.state.userObject.name}, 
            email:{this.state.userObject.email},
            age:{this.state.userObject.age}</Text>}
            <Text style={styles.title}>{JSON.stringify(this.state.userObjectArray)}</Text>}
          <MyComponent onPress={this.funcParent}>

          </MyComponent>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',    
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 10,
  },  
});
