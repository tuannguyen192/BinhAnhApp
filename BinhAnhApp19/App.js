/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import Blink from './Blink';

import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';



export default class App extends Component{  
  render() {
    return (
      <View style={styles.container}>
        <Blink name="Hoang 123456" email="hoang@gmail.com"></Blink>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
 
});
