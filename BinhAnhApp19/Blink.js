import React, { Component } from 'react';
import { Text, View } from 'react-native';

export default class Blink extends Component {
    constructor(props) {
        super(props);
        this.state = { isShowingText: true };
        console.log("Calling constructor");
        // Toggle the state every second
        setInterval(() => {
            console.log("Start changing state");
            this.setState({ isShowingText: !this.state.isShowingText}); 
            // this.setState(previousState => {
            //     return { isShowingText: !previousState.isShowingText };
            // });
        }, 3000);
    }
    componentDidMount() {
        console.log("componentDidMount");
    }
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        console.log("Calling shouldComponentUpdate");
        console.log(`nextProps = ${JSON.stringify(nextProps)}`);        
        console.log(`nextState = ${JSON.stringify(nextState)}`);       
        return true;         
    }
    componentWillUpdate() {        
        console.log("componentWillUpdate");        
    }
    componentDidUpdate() {
        console.log("componentDidUpdate");
    }
    componentWillUnmount() {
        console.log("componentWillUnmount");
    }
    

    render() {
        console.log("call RENDER");
        let display = this.state.isShowingText ? this.props.name : ' ';
        return (
            <Text>{display}</Text>
        );
    }
}