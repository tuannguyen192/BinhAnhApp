/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import PropTypes from 'prop-types';
import EventBridge, {
  enhanceForEventsSupport,
  enhanceForEventsSupportDecorator,
  enhanceForEventsSupportEnhanced,
} from 'react-native-event-bridge';
import EmitterSubscription from 'EmitterSubscription';

export default class App extends Component{
  
  static contextTypes = {
    rootTag: PropTypes.number,
  };
  constructor(props) {
    super(props);
    
  }
  componentDidMount = () => {
    // Register for any kind of event that will be sent from the native side
    this.emitterSubscription = EventBridge.addEventListener(this, (name, info) => {
      alert("Listen for event from Native: '" + name + "' with info: " + JSON.stringify(info));
    });
  }
  componentWillUnmount() {
    if(this.emitterSubscription) {
      this.emitterSubscription.remove();
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Send event between Native and RN</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red'    
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },  
});
