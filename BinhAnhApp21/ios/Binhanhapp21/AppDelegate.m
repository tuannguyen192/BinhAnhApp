/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

#import "AppDelegate.h"

#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import "MSREventBridge.h"

@implementation AppDelegate {
    RCTRootView *rootView;
    UIViewController *rootViewController;
    UINavigationController *navigationViewController;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  NSURL *jsCodeLocation;

  jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];

  rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                      moduleName:@"Binhanhapp21"
                                               initialProperties:nil
                                                   launchOptions:launchOptions];
  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];

  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  rootViewController = [UIViewController new];
  navigationViewController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
  rootViewController.view = rootView;
  self.window.rootViewController = navigationViewController;
  UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]
                                         initWithTitle:@"Send to RN"
                                         style:UIBarButtonItemStylePlain
                                         target:self
                                         action:@selector(sendEventToReactNative:)];
  [rootViewController.navigationItem setRightBarButtonItem:rightBarButtonItem];
  [self.window makeKeyAndVisible];
  return YES;
}
-(void) sendEventToReactNative:(id)sender {
  MSREventBridge *bridge =  (MSREventBridge *)rootView.bridge;
  
  [bridge.viewControllerEventEmitter
   emitEventForView: rootViewController.view
   name:@"eventA" info:@{@"from": @"ios", @"name" : @"Hoang"}];
}
@end
