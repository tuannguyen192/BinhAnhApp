package com.binhanhapp21;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.react.ReactActivity;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;

import net.mischneider.MSREventBridgeInstanceManagerProvider;
import net.mischneider.MSREventBridgeModule;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "Binhanhapp21";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Button btn = new Button(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        btn.setLayoutParams(params);
        btn.setText("Event");
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emitEvent();
            }
        });

        addContentView(btn, params);

//        new CountDownTimer(10000, 1000) {
//            public void onTick(long millisUntilFinished) {
////                emitEvent();
//            }
//            public void onFinish() {
////                Toast.makeText(getApplicationContext(), "DONE", Toast.LENGTH_LONG).show();
//                emitEvent();
//            }
//
//        }.start();
    }

    void emitEvent(){
        Toast.makeText(getApplicationContext(), "aaqq", Toast.LENGTH_LONG).show();
        MSREventBridgeInstanceManagerProvider instanceManagerProvider =
                (MSREventBridgeInstanceManagerProvider)this.getApplicationContext();
        WritableMap map = new WritableNativeMap();
        map.putString("from", "android");
        map.putString("name", "Hoang");
        MSREventBridgeModule.emitEventForActivity(this,
                instanceManagerProvider,
                "eventA", map);
    }
}
