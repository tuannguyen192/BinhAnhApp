export var locations = [
    {        
        latitude: 21.013787,
        longitude: 105.846746,
    },
    {
        latitude: 21.013674,
        longitude: 105.846720,
    },
    {
        latitude: 21.013512,
        longitude: 105.846657,
    },
    {
        latitude: 21.013403,
        longitude: 105.846762,
    },
    {
        latitude: 21.013395,
        longitude: 105.846771,
    },
    {
        latitude: 21.013351,
        longitude: 105.846832,
    },
    {
        latitude: 21.013301,
        longitude: 105.846886,
    },
    {
        latitude: 21.013214,
        longitude: 105.847463,
    },
    {
        latitude: 21.013228,
        longitude: 105.847558,
    },
    {
        latitude: 21.013244,
        longitude: 105.847683,
    },
    {
        latitude: 21.013254,
        longitude: 105.847854,
    },
    {
        latitude: 21.013268,
        longitude: 105.848020,
    },
    {
        latitude: 21.013277,
        longitude: 105.848145,
    },
    {
        latitude: 21.013281,
        longitude: 105.848211,
    },
    {
        latitude: 21.013291,
        longitude: 105.848293,
    },
    {
        latitude: 21.013301,
        longitude: 105.848435,
    },
    {
        latitude: 21.013315,
        longitude: 105.848628,
    },
    {
        latitude: 21.013338,
        longitude: 105.848817,
    },
    {
        latitude: 21.013351,
        longitude: 105.848913,
    },
    {
        latitude: 21.013370,
        longitude: 105.849113,
    },
    {
        latitude: 21.013376,
        longitude: 105.849220,
    },
    {
        latitude: 21.013381,
        longitude: 105.849278,
    },
    {
        latitude: 21.013387,
        longitude: 105.849355,
    },
    {
        latitude: 21.013391,
        longitude: 105.849455,
    },
    {
        latitude: 21.013398,
        longitude: 105.849553,
    },
    {
        latitude: 21.013404,
        longitude: 105.849622,
    },
    {
        latitude: 21.013410,
        longitude: 105.849700,
    },
    {
        latitude: 21.013415,
        longitude: 105.849797,
    },
    {
        latitude: 21.013417,
        longitude: 105.849850,
    },
    {
        latitude: 21.013420,
        longitude: 105.849887,
    },
    {
        latitude: 21.013472,
        longitude: 105.849885,
    },
    {
        latitude: 21.013535,
        longitude: 105.849885,
    },
    {
        latitude: 21.013681,
        longitude: 105.849882,
    },
    {
        latitude: 21.013735,
        longitude: 105.849880,
    },    
];
export const getRotation = (lat2,long2,lat1,long1) => {
    rotation = Math.atan2(lat2 - lat1, long2 - long1);
    rotation = rotation * (180 / Math.PI);
    rotation = (rotation + 360) % 360;
    return 360 - rotation;
}
export const getVelocity = (lat2, long2,lat1,long1,timeStamp2,timeStamp1) => {
    const p = 0.017453292519943295;    // Math.PI / 180
    const c = Math.cos;
    var a = 0.5 - c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) *
        (1 - c((long2 - long1) * p)) / 2;
    let distance = 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km    
    return distance / (timeStamp2-timeStamp1);
}
