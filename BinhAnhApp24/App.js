/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Animated, Easing
} from 'react-native';

import MapView, { Polyline, Marker } from 'react-native-maps';
import GeoCoder from 'react-native-geocoder';

import LocationSearch from './components/LocationSearch';
import { locations, getRotation, getVelocity } from './MapData';
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentLocation: locations[0],
      markerPath: require('./images/car.png'),
      polylineCoordinates:[]      
    };
    this.initialRegion = {
      latitude: 21.013787,
      longitude: 105.846746,
      latitudeDelta: 0.00322,//Difference between the minimun and the maximum latitude/longitude
      longitudeDelta: 0.00421,
    };
  }
  loadDataFromServer = () => {
    var currentIndex = 0;
    setInterval(() => {
      if (currentIndex > locations.length - 1) {
        return;
      }      
      let currentLocation = locations[currentIndex];
      currentLocation.timeStamp = Date.now() / 1000;
      if (currentIndex > 0) {
        var previousIndex = currentIndex - 1;
        var previousLocation = locations[previousIndex];
        currentLocation.rotation = getRotation(
          currentLocation.latitude, currentLocation.longitude,
          previousLocation.latitude, previousLocation.longitude
        );
        currentLocation.velocity = getVelocity(
          currentLocation.latitude, currentLocation.longitude,
          previousLocation.latitude, previousLocation.longitude,
          currentLocation.timeStamp, previousLocation.timeStamp
        );
        this.setState({ 
          polylineCoordinates: this.state.polylineCoordinates.concat(currentLocation)
        });
      }
      console.log(`currentLocation = ${JSON.stringify(currentLocation)}`);      
      this.setState({ currentLocation });
      if (currentLocation.velocity > 0.017) {
        this.setState({markerPath: require('./images/carRed.png')});
      } else {
        this.setState({ markerPath: require('./images/car.png') });
      }
      this.refs.mapView.animateToCoordinate(currentLocation, 1000);      
      currentIndex = currentIndex + 1;
    }, 1000);
  }
  componentDidMount() {
    this.loadDataFromServer();
  }

  _onRegionChange = (region) => {
    this.setState({ geocodePosition: null });
    this.timeoutId = setTimeout(async () => {
      try {
        const response = await GeoCoder.geocodePosition({
          lat: region.latitude,
          lng: region.longitude,
        });
        this.setState({ geocodePosition: response[0] });
        // alert(`this.state.geocodePosition = 
        // ${JSON.stringify(this.state.geocodePosition)}`); 

      } catch (err) {
        // console.log(err);
      }
    }, 2000);
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <MapView ref={"mapView"}
          style={styles.fullScreenMap}
          initialRegion={this.initialRegion}
          onRegionChange={this._onRegionChange} 
          showsUserLocation={true}
          userLocationAnnotationTitle="RN MapView example"
          showsMyLocationButton={true}
          followsUserLocation={true}          
          showsTraffic={true}     
          liteMode={true}//android 
          onMapReady={()=>{

          }}
          onRegionChangeComplete={() => {

          }}
          onMarkerPress={() => {
            alert("You pressed the Marker");
          }}
        >
          <Marker
            key={123}
            coordinate={this.state.currentLocation}>
              <Animated.Image
                source={this.state.markerPath}                
                style={{
                  transform: [{ rotate: `${this.state.currentLocation.rotation}deg` }],
                  //transform: [{ rotate: `30deg` }],
                  width: 50, height: 25
                }}
              />
          </Marker>          
          <Polyline
            coordinates={this.state.polylineCoordinates}
            strokeColor="steelblue" 
            strokeWidth={3}
          />
        </MapView>
        <LocationSearch
          value={
            this.state.geocodePosition &&
            (this.state.geocodePosition.feature ||
              this.state.geocodePosition.formattedAddress)
          }
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  fullScreenMap: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
});
