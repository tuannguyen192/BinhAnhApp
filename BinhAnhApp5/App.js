/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

import { 
  insertTblDevice,
  updateTblDevice,
  deleteAllTblDevice,
  queryAllTblDevice, 
  deleteTblDeviceById,
  checkTblExists,
  checkColumnExist,
  addColumn
   } from './Database/DeviceSchema';
import { createAllTables } from './Database/DBManager';
import {
  insertTblHistory,
  queryAllTblHistory,
  deleteAllTblHistory,
  deleteTblHistoryById,
  updateTblHistory,
  queryHistoryAndDevice,
} from './Database/HistorySchema';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload11,\n' +
    'Shake or press menu button for dev menu',
});

//type Props = {};
export default class App extends Component {
  componentDidMount() {
    //addColumn(); /* alter table add new column*/
    checkColumnExist('tblDevice','12deviceName'); /* list all column name of table to check column is exists or not*/
    //checkTblExists('tblHistory'); /* check table exists or not*/
    //createAllTables();
    //insertTblDevice("Device 1", "Đây là device 1", "https://upload.wikimedia.org/wikipedia/commons/7/71/Expl1825_-_Flickr_-_NOAA_Photo_Library.jpg");
    //queryAllTblDevice();
    //updateTblDevice(1,"Device 10")
    //deleteTblDeviceById("1");
    //deleteAllTblDevice();
    //insertTblHistory("Pho B, Duong C", 12342.21, 232132.32, 1);
    //queryAllTblHistory();
    //deleteAllTblHistory();
    //deleteTblHistoryById(1);
    //updateTblHistory(1,"Pho M, Duong N")
    //queryHistoryAndDevice(1)
  } 
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native!
        </Text>
        <Text style={styles.instructions}>
          To get started, edit App.js
        </Text>
        <Text style={styles.instructions}>
          {instructions}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
