import { openDatabase } from 'react-native-sqlite-storage';
import { database, convertToSQLUpdateString } from './DBManager';

const checkTblExists = (tblName)=>{
    database.transaction(tx => {
        const sqlInsert = `SELECT * FROM sqlite_master WHERE type='table' AND name='${tblName}';`;
        //alert(sqlInsert);
        tx.executeSql(sqlInsert, [], (transaction, resultSet) => {
            alert(`table is = ${JSON.stringify(resultSet.rows.length)}`);
        }, (transaction, error) => {
            alert(`insert data to tblDevice failed, error = ${error}`);
        });
    });
}

const checkColumnExist = (tblName, colName)=>{
    database.transaction(tx => {
        const sqlInsert = `PRAGMA table_info(${tblName})`;
        //alert(sqlInsert);
        tx.executeSql(sqlInsert, [], (transaction, resultSet) => {
            for (let i = 0; i < resultSet.rows.length; i++) {
                let row = resultSet.rows.item(i);
                //console.log(`row = ${JSON.stringify(row.name)}`);
                if(row.name === colName){
                    alert(1)
                    return
                }
            }
            alert(0)
            return
                //alert(`queryAllTblDevice successfully, resultSet = ${JSON.stringify(resultSet)}`);
            }, (transaction, error) => {
                alert(`queryAllTblDevice failed, error = ${JSON.stringify(error)}`);
            });
    });
}

const addColumn = ()=>{
    database.transaction(tx => {
        const sqlInsert = `ALTER TABLE tblDevice ADD COLUMN deviceColor TEXT`;
        //alert(sqlInsert);
        tx.executeSql(sqlInsert, [], (transaction, resultSet) => {
            alert(`table is = ${JSON.stringify(resultSet)}`);
        }, (transaction, error) => {
            alert(`insert data to tblDevice failed, error = ${error}`);
        });
    });
}

const insertTblDevice = (deviceName, description, deviceURL) => {
    database.transaction(tx => {
        const sqlInsert = `INSERT INTO tblDevice(deviceName, description, deviceURL) values("${deviceName}", "${description}", "${deviceURL}")`;
        alert(sqlInsert);
        tx.executeSql(sqlInsert, [], (transaction, resultSet) => {
            alert(`insert data to tblDevice successfully, resultSet = ${JSON.stringify(resultSet)}`);
        }, (transaction, error) => {
            alert(`insert data to tblDevice failed, error = ${error}`);
        });
    });
}
const queryAllTblDevice = () => {
    database.transaction(tx => {
        const sqlInsert = `SELECT * FROM tblDevice`;
        alert(sqlInsert);
        tx.executeSql(sqlInsert, [], (transaction, resultSet) => {            
        for (let i = 0; i < resultSet.rows.length; i++) {
            let row = resultSet.rows.item(i);
            alert(`Device name: ${row.deviceName}, description: ${row.description}, deviceURL: ${row.deviceURL}`);
        }
            alert(`queryAllTblDevice successfully, resultSet = ${JSON.stringify(resultSet)}`);
        }, (transaction, error) => {
            alert(`queryAllTblDevice failed, error = ${JSON.stringify(error)}`);
        });
    });
}
const deleteAllTblDevice = () => {
    database.transaction(tx => {
        const sqlDeleteAll = `DELETE FROM tblDevice`;
        alert(sqlDeleteAll);
        tx.executeSql(sqlDeleteAll, [], (transaction, resultSet) => {                    
            alert(`Delete all tblDevice successfully, resultSet = ${JSON.stringify(resultSet)}`);
        }, (transaction, error) => {
            alert(`queryAllTblDevice failed, error = ${JSON.stringify(error)}`);
        });
    });
}
const updateTblDevice = (id, updateParams) => {
    database.transaction(tx => {
        const sqlUpdate = `UPDATE tblDevice SET deviceName = "${updateParams}" WHERE tblDevice.id = ${id}`;
        alert(sqlUpdate);
        tx.executeSql(sqlUpdate, [], (transaction, resultSet) => {                    
            alert(`Update tblDevice successfully, resultSet = ${JSON.stringify(resultSet)}`);
        }, (transaction, error) => {
            alert(`update TblDevice failed, error = ${JSON.stringify(error)}`);
        });
    });
}
const deleteTblDeviceById = (id) => {
    database.transaction(tx => {
        const sqlDeleteByID = `DELETE FROM tblDevice WHERE tblDevice.id = ${id}`;
        alert(sqlDeleteByID);
        tx.executeSql(sqlDeleteByID, [], (transaction, resultSet)=>{
            alert(`Delete tblDevice id successfully, resultSet = ${JSON.stringify(resultSet)}`);
        },(transaction, error)=>{
            alert(`Delete TblDevice failed, error = ${JSON.stringify(error)}`);
        });
    });
}


export { 
    insertTblDevice,
    deleteAllTblDevice,
    updateTblDevice,
    queryAllTblDevice,
    deleteTblDeviceById,
    checkTblExists,
    checkColumnExist,
    addColumn
};