/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
// API key: AIzaSyAKkZP2xgddNhJLm6TXSMdvUhDcaX-Wz9s
// sender id: 829332331308

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, 
  TouchableOpacity
} from 'react-native';
import PushNotification from 'react-native-push-notification';

PushNotification.configure({

  // (optional) Called when Token is generated (iOS and Android)
  onRegister: function(token) {
      console.log( 'TOKEN:', token );
  },

  // (required) Called when a remote or local notification is opened or received
  onNotification: function(notification) {
      console.log( 'NOTIFICATION:', notification );

      // process the notification

      // required on iOS only (see fetchCompletionHandler docs: https://facebook.github.io/react-native/docs/pushnotificationios.html)
      notification.finish(PushNotificationIOS.FetchResult.NoData);
  },

  // ANDROID ONLY: GCM Sender ID (optional - not required for local notifications, but is need to receive remote push notifications)
  senderID: "829332331308",

  // IOS ONLY (optional): default: all - Permissions to register.
  permissions: {
      alert: true,
      badge: true,
      sound: true
  },

  // Should the initial notification be popped automatically
  // default: true
  popInitialNotification: true,

  /**
    * (optional) default: true
    * - Specified if permissions (ios) and token (android and ios) will requested or not,
    * - if not, you must call PushNotificationsHandler.requestPermissions() later
    */
  requestPermissions: true,
});

export default class App extends Component {

  _pressPush = ()=>{
    PushNotification.localNotification({
      title: "My Notification Title",
      message: "My Notification Message",
      playSound: true,
      soundName: 'default',
    })
  }
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress = {this._pressPush}
        >
          <Text>Push</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
