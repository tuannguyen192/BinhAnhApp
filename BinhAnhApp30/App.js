/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {log: ""};
  }
  componentDidMount(){    
    var ws = new WebSocket('ws://Nguyens-iMac.local:8082');
    ws.onopen = () => {
      // connection opened
      ws.send('client opened'); // send a message
      // var objectsToSend = [{ name: 'hoang1', email: 'email1@gmail.com' }, 
      // { name: 'hoang2', email: 'email2@gmail.com' }];
      // // websocketClient.send(BSON.serialize(objectsToSend));           
      // ws.send(JSON.stringify(objectsToSend));//good
    };

    ws.onmessage = (e) => {
      // a message was received      
      this.setState({ log: `client received: ${JSON.stringify(e.data)}`});
    };

    ws.onerror = (e) => {      
      console.log(`socket error: ${JSON.stringify(e.message)}`);      
    };

    ws.onclose = (e) => {
      // connection closed
      console.log(`connection closed: ${e.code}, reason: ${e.reason}`);      
    };
    
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={{ fontSize: 25 }}>
          Websocket examples
        </Text>
        <Text style={{ fontSize: 22}}>
          {this.state.log}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'    
  },  
});
