/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text, TouchableOpacity,
  View
} from 'react-native';
import store from './store';
import store2 from './store';
import {observer} from 'mobx-react';


@observer
export default class App extends Component{
  constructor(props) {
    super(props);
    store.addNewTodo({
      id: 1,
      todoName: "Develop GMSMapView in IOS",
      description: "Write a MapView and add to a new ViewController"
    });
    store2.addNewTodo({
      id: 2,
      todoName: "2Develop GMSMapView in IOS",
      description: "2Write a MapView and add to a new ViewController"
    });
    alert(`store2.todos = ${JSON.stringify(store2.todos)}`)
  }
  async componentDidMount() {    

  }
  _onPressInsert = () => {
    store.addNewTodo({
      id: 1,
      todoName: "Develop GMSMapView in IOS",
      description: "Write a MapView and add to a new ViewController"
    });   
  }
  _onPressUpdate = () => {
    store.updateTodoWithId(1,{      
      todoName: "Develop MapView in Android",
      description: "Write a MapView and add to an Activity"
    });    
  }
  _onPressDelete = () => {
    store.deleteTodoWithId(1);    
  }
  
  render() {
    const {todos} = store;
    return (
      <View style={styles.container}>
        <Text style={styles.title}>MobX</Text>        
        <TouchableOpacity onPress={this._onPressInsert}>
          <Text style={styles.button}>Insert a Todo</Text>
        </TouchableOpacity>        
        <TouchableOpacity onPress={this._onPressUpdate}>
          <Text style={styles.button}>Update a Todo</Text>
        </TouchableOpacity>        
        <TouchableOpacity onPress={this._onPressDelete}>
          <Text style={styles.button}>Delete a Todo</Text>
        </TouchableOpacity>        
        {todos.length && <Text style={styles.title}>todos={JSON.stringify(todos)}</Text>}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',    
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 10,
  },  
  button: {
    backgroundColor: 'green', 
    color: 'white', padding: 20, fontSize: 20,
    margin: 10
  }
});
