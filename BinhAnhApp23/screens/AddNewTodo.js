import React, { Component } from 'react';
import {
    Container,
    Content,
    Form,
    Item,
    Input,
    Button,
    Text,
} from 'native-base';
import { Alert, ActivityIndicator } from 'react-native';
import { addNewTodo, fetchTodo } from '../store';
export default class AddNewTodo extends Component {
    static navigationOptions = {
        title: "Thêm Todo mới",
    };
    _handleAddPress() {        
        if (this.state.todoName.length > 0) {
            this.setState({ loading: true });
            fetchTodo(this.state.todoName)
                .then(todo => {
                    //Gọi action thêm todo mới                    
                    addNewTodo(this.state.todoName, todo);
                    this.setState({ loading: false });
                    this.props.navigation.goBack();
                })
                .catch((error) => {
                    alert(`Error in creacting new todo: ${error}`);
                    this.setState({ loading: false });
                });
        }
    }
    constructor(props) {
        super(props);           
        this.state = {
            url: '',
            loading: false,
        };
    }
    render() {
        return (
            <Container style={{ padding: 10 }}>
                <Content>
                    <Form>
                        <Item>
                            <Input
                                autoCapitalize="none"
                                autoCorrect={false}
                                placeholder={"Nhập URL của Todo"}
                                onChangeText={url => this.setState({ url })}
                                value={this.state.todoName}
                            />
                        </Item>
                        <Button
                            block
                            style={{ marginTop: 20 }}
                            onPress={this._handleAddPress.bind(this)}
                        >
                            {this.state.loading && (
                                <ActivityIndicator color="white" style={{ margin: 10 }}
                                />
                            )}
                            <Text>Thêm Todo mới</Text>
                        </Button>
                    </Form>
                </Content>
            </Container>
        );
    }
}