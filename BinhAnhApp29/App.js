/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
//Create font online
import React, { Component } from 'react';
import Icon from 'react-native-vector-icons';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={{ fontSize: 22, fontFamily: 'FontAwesome'}}>
          The quick brown fox jumps over the lazy dog
        </Text>
        <Text style={{ 
          fontSize: 50, 
          fontFamily: 'FontAwesome', 
          color: 'steelblue',
          margin:20 }}>
          l
        </Text>
        <Text style={{
          fontSize: 50,
          fontFamily: 'MyFont-normal',
          color: 'steelblue',
          margin: 20
        }}>
          a
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',    
  },
});
