import { AppRegistry } from 'react-native';
import {TabNavigator} from 'react-navigation';
import HomeComponent from './components/HomeComponent';
import DeviceComponet from './components/DeviceComponet';
import MapComponent from './components/MapComponent';
import {Home, Device, Map} from './screensName';

let routerConfig = {
    Home: {
        screen: HomeComponent
    },
    Device: {
        screen: DeviceComponet
    },
    Map: {
        screen: MapComponent
    }
};

let tabNavigatorConfig = {
    tabBarPosition: 'bottom',
    animationEnabled: true,
    swipeEnabled: true,
    tabBarOptions: {
        showIcon: true,
        showLabel: true,
        activeTintColor: 'steelblue',
        labelStyle: {
            fontSize: 13,
        },
        style: {
            backgroundColor: 'gray',
            padding: -10
        },    
    }
    
};
const App = TabNavigator(routerConfig, tabNavigatorConfig);

AppRegistry.registerComponent('BinhAnhApp2', () => App);
