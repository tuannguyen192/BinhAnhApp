import React,{Component} from 'react';
import {Icon, Text, View} from 'native-base';
import {StyleSheet} from 'react-native';

export default class DeviceComponent extends Component{
    static navigationOptions = {
        tabBarIcon: ({tintColor})=>{
            return <Icon name = 'ios-car-outline' style={{color: tintColor}}/>
        }
    }
    constructor(props){
        super(props);
    }

    render(){
        return(
            <View style={{alignItems:'center', justifyContent: 'center', flex: 1}}>
                <Text style={{fontWeight: 'bold', fontSize: 30}}>This is Device Screen</Text>
            </View>
        );
    }
}