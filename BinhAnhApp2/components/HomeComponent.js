import React,{Component} from 'react';
import {Icon, Text, View} from 'native-base';
import {StyleSheet} from 'react-native';

export default class HomeComponent extends Component{
    static navigationOptions = {
        tabBarIcon: ({tintColor})=>{
            return <Icon name = 'ios-home-outline' style={{color: tintColor}}/>
        }
    }
    constructor(props){
        super(props);
    }

    render(){
        return(
            <View style={{alignItems:'center', justifyContent: 'center', flex: 1}}>
                <Text style={{fontWeight: 'bold', fontSize: 30}}>This is Home Screen</Text>
            </View>
        );
    }
}