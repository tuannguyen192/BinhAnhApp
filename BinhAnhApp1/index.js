import { AppRegistry } from 'react-native';
import {StackNavigator} from 'react-navigation'
import DeviceComponent from './components/DeviceComponent';
import HistoricalComponent from './components/HistoricalComponent';

import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

const Navigator = StackNavigator(
    {
        DeviceComponent: {screen: DeviceComponent},
        HistoricalComponent: {screen: HistoricalComponent}
    }
)

AppRegistry.registerComponent('BinhAnh', () => Navigator);
