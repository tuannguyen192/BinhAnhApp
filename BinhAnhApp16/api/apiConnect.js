import React ,{Component} from 'react';

const addDeviceApi = "http://Tuans-MacBook-Pro.local:3000/device/add_new_device/";
const queryAllDevicesApi = "http://Tuans-MacBook-Pro.local:3000/device/get_all_devices";
const deleteDeviceApi = "http://Tuans-MacBook-Pro.local:3000/device/delete_a_device";
const updateDeviceApi = "http://Tuans-MacBook-Pro.local:3000/device/update_a_device";
const registerAPI = "http://localhost:3001/insert_user";

// async function addNewDevice1(param) {
//     console.log(JSON.stringify(param))
//     try{
//         let response = await fetch(addDeviceApi,{
//             method: 'POST',
//             headers: {
//                 'Accept': 'application/json',
//                 'Content-Type': 'application/json'
//             },
//             body: JSON.stringify({
//                 deviceName: 'yourValue',
//                 description: 'yourOtherValue',
//                 deviceURL: 'https://bcdcog.com/wp-content/uploads/2016/05/profile-default-02.png',
//             })
//         })
//         let responseJson = await response.json();
//         return(responseJson.result)
//     } catch (error) {
//         //console.error(`Error is : ${error}`);
//     }
// }

async function register() {
    try{
        let response = await fetch(registerAPI,{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: 'Tuan',
                datebirth: '13/12/1992kkkkk',
                username: 'foxbeel',
                password: '123456'
            })
        })
        let responseJson = await response.json();
        return(responseJson.result)
    } catch (error){
        alert(error);
    }
}

async function queryAllDevices() {
    try {
        let response = await fetch(queryAllDevicesApi);
        let responseJson = await response.json();
        return (responseJson.data);
    } catch (error){
        console.log(`error == ${JSON.stringify(error)}`);
    }
}
async function addNewDevice(params) {
    //console.log(JSON.stringify(params));
    try {
        let response = await fetch(addDeviceApi, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'charset=UTF-8',
            },
            body: JSON.stringify(params),
            //body: `deviceName=${params.deviceName}&description=${params.description}&deviceURL=${params.deviceURL}`
        });
        let responseJson = await response.json();
        return responseJson.result; 
    } catch (error) {
        console.log(`Error is : ${error}`);
    }
}

// function addNewDevice(params) {
//     return fetch(addDeviceApi, {
//         method: 'POST',
//         headers: {
//           Accept: 'application/json',
//           'Content-Type': 'application/json',
//         },
//         body: JSON.stringify(params),
//       })
//       .then((response) => response.json())
//       .then((responseJson) => {
//         return responseJson.result;
//       })
//       .catch((error) => {
//         console.error(error);
//       });
//   }

async function deleteDevice(params) {
    //console.log(JSON.stringify(params));
    try {
        let response = await fetch(deleteDeviceApi, {
            method: 'DELETE',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            //body: JSON.stringify(params),
            body: `deviceId=${params}`
        });
        let responseJson = await response.json();
        return responseJson.result; 
    } catch (error) {
        console.log(`Error is : ${error}`);
    }
}

async function updateDevice(deviceID, device) {
    //console.log(JSON.stringify(params));
    try {
        let response = await fetch(deleteDeviceApi, {
            method: 'PUT',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            //body: JSON.stringify(params),
            body: `deviceId=${deviceID}&`
        });
        let responseJson = await response.json();
        return responseJson.result; 
    } catch (error) {
        console.log(`Error is : ${error}`);
    }
}

export {
    addNewDevice,
    queryAllDevices,
    deleteDevice,
    updateDevice,
    register
}