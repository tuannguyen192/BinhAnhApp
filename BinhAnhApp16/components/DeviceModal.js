import React, { Component } from 'react';
import {
    View, StyleSheet
} from 'react-native';
import {
    Body, Container, Button,
    Content, Right, Text, Item, Input
} from 'native-base';
import Modal from "react-native-modal";
export default class DeviceModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            device: {},
            isVisible: false,
            actionType:'insert', //insert or update
        }
    }
    showModal = (actionType, device) => {
        this.setState({ isVisible: true });
        this.setState({ actionType });
        if (actionType === 'update') {
            this.setState({ device: device });
        } else if (actionType === 'insert') {
            this.setState({ device: {} });
        }
    }
    hideModal = () => {
        this.setState({ isVisible: false })
    }
    render() {
        return (
            <Modal
                isVisible={this.state.isVisible}
                onBackdropPress={this.hideModal}
                avoidKeyboard={true}
            >
                <View style={styles.modal}>
                    <Item style={styles.itemInput}>
                        <Input placeholder="Enter device's name"
                            onChangeText={text => {
                                let updatingDevice = this.state.device;
                                updatingDevice.deviceName = text;
                                this.setState({ device: updatingDevice });
                            }}
                            value={this.state.device.deviceName}
                            style={styles.input}
                        />
                    </Item>
                    <Item style={styles.itemInput}>
                        <Input placeholder="Enter description"
                            onChangeText={text => {
                                let updatingDevice = this.state.device;
                                updatingDevice.description = text;
                                this.setState({ device: updatingDevice });
                            }}
                            value={this.state.device.description}
                            style={styles.input}
                            // keyboardType='numeric'
                        />
                    </Item>
                    <Button block
                        style={styles.button}
                        onPress={() => {     
                            if(!this.state.device.description || this.state.device.description.length < 3){
                                alert('Description must be >= 3 characters');
                                return;
                            }
                            if(!this.state.device.deviceName || this.state.device.deviceName.length < 3){
                                alert('deviceName must be >= 3 characters');
                                return;
                            }                                                                           
                            if (this.state.actionType === 'update') {
                                this.props.updateAnItem(this.state.device.id, this.state.device);    
                            } else if (this.state.actionType === 'insert') {
                                this.props.insertAnItem(this.state.device);    
                            }
                            this.hideModal();                            
                        }}
                    >
                        <Text>Save</Text>
                    </Button>
                </View>
            </Modal>);
    }
}
const styles = StyleSheet.create({
    modal: {
        backgroundColor: 'white',
        height: 230,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 7
    },
    input: {
        marginHorizontal: 20,
        paddingLeft: 10,
        borderColor: 'rgb(100,121,133)',
        borderWidth: 1
    },
    itemInput: {
        marginTop: 10
    },
    button: {
        backgroundColor: 'darksalmon',
        marginHorizontal: 20,
        marginTop: 20,
        borderRadius: 5
    }
});