import { openDatabase } from 'react-native-sqlite-storage';
import { database, convertToSQLUpdateString } from './DBManager';

const insertTblDevice = (device) => {
    database.transaction(tx => {
        const sqlInsert = `INSERT INTO tblDevice(deviceName, description, deviceURL) values("${device.deviceName}", "${device.description}", "${device.deviceURL}")`;
        // alert(sqlInsert);
        tx.executeSql(sqlInsert, [], (transaction, resultSet) => {
            // alert(`insert data to tblDevice successfully, resultSet = ${JSON.stringify(resultSet)}`);
        }, (transaction, error) => {
            // alert(`insert data to tblDevice failed, error = ${error}`);
        });
    });
}
const queryAllTblDevice = (callback) => {
    database.transaction(tx => {
        let allDevices = [];
        const sqlInsert = `SELECT * FROM tblDevice`;
        //alert(sqlInsert);
        tx.executeSql(sqlInsert, [], (transaction, resultSet) => {            
        for (let i = 0; i < resultSet.rows.length; i++) {
            let row = resultSet.rows.item(i);
            //alert(`Device name: ${row.deviceName}, description: ${row.description}, deviceURL: ${row.deviceURL}`);
            let device = {
                id: row.id,
                deviceName: row.deviceName,
                description: row.description,
                deviceURL: row.deviceURL,
            }
            allDevices.push(device);
        }
         callback(allDevices);
            //alert(`queryAllTblDevice successfully, resultSet = ${JSON.stringify(resultSet)}`);
        }, (transaction, error) => {
            console.log(`queryAllTblDevice failed, error = ${JSON.stringify(error)}`);
        });
    });
}
const deleteAllTblDevice = () => {
    database.transaction(tx => {
        const sqlDeleteAll = `DELETE FROM tblDevice`;
        alert(sqlDeleteAll);
        tx.executeSql(sqlDeleteAll, [], (transaction, resultSet) => {                    
            alert(`Delete all tblDevice successfully, resultSet = ${JSON.stringify(resultSet)}`);
        }, (transaction, error) => {
            alert(`queryAllTblDevice failed, error = ${JSON.stringify(error)}`);
        });
    });
}
const updateTblDevice = (id, updateParams) => {
    database.transaction(tx => {
        const sqlUpdate = `UPDATE tblDevice SET deviceName = "${updateParams.deviceName}", description = "${updateParams.description}" WHERE tblDevice.id = ${id}`;
        //alert(sqlUpdate);
        tx.executeSql(sqlUpdate, [], (transaction, resultSet) => {                    
            console.log(`Update tblDevice successfully, resultSet = ${JSON.stringify(resultSet)}`);
        }, (transaction, error) => {
            console.log(`update TblDevice failed, error = ${JSON.stringify(error)}`);
        });
    });
}
const deleteTblDeviceById = (id) => {
    database.transaction(tx => {
        const sqlDeleteByID = `DELETE FROM tblDevice WHERE tblDevice.id = ${id}`;
        //alert(sqlDeleteByID);
        tx.executeSql(sqlDeleteByID, [], (transaction, resultSet)=>{
            //alert(`Delete tblDevice id successfully, resultSet = ${JSON.stringify(resultSet)}`);
        },(transaction, error)=>{
            //alert(`Delete TblDevice failed, error = ${JSON.stringify(error)}`);
        });
    });
}


export { 
    insertTblDevice,
    deleteAllTblDevice,
    updateTblDevice,
    queryAllTblDevice,
    deleteTblDeviceById
};