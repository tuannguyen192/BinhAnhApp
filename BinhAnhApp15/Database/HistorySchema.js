import { openDatabase } from 'react-native-sqlite-storage';
import { database, convertToSQLUpdateString } from './DBManager';

const insertTblHistory = (locationName, lat, long, deviceId)=>{
    database.transaction(tx => {
        const sqlInsert = `INSERT INTO tblHistory(locationName, lat, long, deviceId) values("${locationName}", ${lat}, ${long},${deviceId})`;
        console.log(sqlInsert);
        tx.executeSql(sqlInsert, [], (transaction, resultSet) => {
            alert(`insert data to tblHistory successfully, resultSet = ${JSON.stringify(resultSet)}`);
        }, (transaction, error) => {
            alert(`insert data to tblHistory failed, error = ${error}`);
        });
    });
};

const queryAllTblHistory = () => {
    database.transaction(tx => {
        const sqlInsert = `SELECT * FROM tblHistory`;
        alert(sqlInsert);
        tx.executeSql(sqlInsert, [], (transaction, resultSet) => {            
        for (let i = 0; i < resultSet.rows.length; i++) {
            let row = resultSet.rows.item(i);
            console.log(`Location Name: ${row.locationName}, Lat: ${row.lat}, Long: ${row.long}, DeviceId: ${row.deviceId}`);
        }
            alert(`queryAllTblHistory successfully, resultSet = ${JSON.stringify(resultSet)}`);
        }, (transaction, error) => {
            alert(`queryAllTblHistory failed, error = ${JSON.stringify(error)}`);
        });
    });
}

const deleteAllTblHistory = () =>{
    database.transaction(tx =>{
        const sqlQuery = `DELETE FROM tblHistory`
        alert(sqlQuery);
        tx.executeSql(sqlQuery, [], (transaction, resultSet) => {                    
            alert(`Delete all tblHistory successfully, resultSet = ${JSON.stringify(resultSet)}`);
        }, (transaction, error) => {
            alert(`Delete all tblHistory failed, error = ${JSON.stringify(error)}`);
        });
    });
}

const deleteTblHistoryById = (id) => {
    database.transaction(tx => {
        const sqlDeleteByID = `DELETE FROM tblHistory WHERE tblHistory.id = ${id}`;
        alert(sqlDeleteByID);
        tx.executeSql(sqlDeleteByID, [], (transaction, resultSet)=>{
            alert(`Delete tblHistory id successfully, resultSet = ${JSON.stringify(resultSet)}`);
        },(transaction, error)=>{
            alert(`Delete tblHistory failed, error = ${JSON.stringify(error)}`);
        });
    });
}

const updateTblHistory = (id, updateParams) => {
    database.transaction(tx => {
        const sqlUpdate = `UPDATE tblHistory SET locationName = "${updateParams}" WHERE tblHistory.id = ${id}`;
        alert(sqlUpdate);
        tx.executeSql(sqlUpdate, [], (transaction, resultSet) => {                    
            alert(`Update tblHistory successfully, resultSet = ${JSON.stringify(resultSet)}`);
        }, (transaction, error) => {
            alert(`update tblHistory failed, error = ${JSON.stringify(error)}`);
        });
    });
}

const queryHistoryAndDevice = (deviceID)=>{
    database.transaction(tx => {
        const sqlInsert = `SELECT * FROM tblHistory,tblDevice WHERE tblDevice.id = tblHistory.deviceId AND tblDevice.id = ${deviceID}`;
        alert(sqlInsert);
        tx.executeSql(sqlInsert, [], (transaction, resultSet) => {            
        for (let i = 0; i < resultSet.rows.length; i++) {
            let row = resultSet.rows.item(i);
            console.log(`Location Name: ${JSON.stringify(row)}`);
        }
            alert(`queryAllTblHistory successfully, resultSet = ${JSON.stringify(resultSet)}`);
        }, (transaction, error) => {
            alert(`queryAllTblHistory failed, error = ${JSON.stringify(error)}`);
        });
    });
}

export {
    insertTblHistory,
    queryAllTblHistory,
    deleteAllTblHistory,
    deleteTblHistoryById,
    updateTblHistory,
    queryHistoryAndDevice,
};