import { openDatabase } from 'react-native-sqlite-storage';

var database = openDatabase({ name: 'sqliteExample.db', location: 'default' }, () => {
    // alert(`connect DB is ok`);
}, (error) => {
    // alert(`connect DB is failed: ${error}`);
});
//CREATE TABLE IF NOT EXISTS tblDevice(id INTEGER, deviceName TEXT,description TEXT, deviceURL TEXT)
//CREATE TABLE IF NOT EXISTS tblHistory(dateTime INTEGER,locationName TEXT, lat NUMBER,long NUMBER, deviceId INTEGER)
const createAllTables =() => {
    database.transaction(tx => {
        const sqlCreateTblDevice = `CREATE TABLE IF NOT EXISTS tblDevice(id INTEGER PRIMARY KEY, deviceName TEXT,description TEXT, deviceURL TEXT);`;
        const sqlCreateTblHistory = `CREATE TABLE IF NOT EXISTS tblHistory(id INTEGER PRIMARY KEY, dateTime INTEGER,locationName TEXT, lat NUMBER,long NUMBER, deviceId INTEGER);`;
        // let sqlCreateAllTable = sqlCreateTblDevice + sqlCreateTblHistory;
        //console.log(sqlCreateTblDevice);
        tx.executeSql(sqlCreateTblDevice, [], (transaction, resultSet) => {
            //alert(`Create tblDevice successfully, resultSet = ${JSON.stringify(resultSet)}`);
        }, (transaction, error) => {
            //alert(`Create tblDevice failed, error = ${error}`);
        });
        tx.executeSql(sqlCreateTblHistory, [], (transaction, resultSet) => {
            //alert(`Create TblHistory successfully, resultSet = ${JSON.stringify(resultSet)}`);
        }, (transaction, error) => {
            //alert(`Create TblHistory failed, error = ${error}`);
        });
    });
}


export { database, createAllTables };
