import React, { Component } from 'react';
import {
    FlatList, Text, View, SafeAreaView, Image, StyleSheet,
    Alert,
    TouchableOpacity
} from 'react-native';
import {Button, Icon} from 'native-base';
import Swipeout from 'react-native-swipeout';
import DeviceModal from './DeviceModal';
import {createAllTables} from '../Database/DBManager';
import {queryAllTblDevice, updateTblDevice, deleteTblDeviceById, insertTblDevice} from '../Database/DeviceSchema';

var showMyModal = ()=>{};
class MyListItem extends Component {
    constructor(props) {
        super(props);
    }
    _handleDelete = () => {
        Alert.alert(
            'Alert',
            'Are you sure you want to delete ?',
            [
                { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                {
                    text: 'Yes', onPress: () => {
                        this.props.deleteAnItem(this.props.item.id);
                    }
                },
            ],
            { cancelable: true }
        );
    }
    render() {
        const swipeSettings = {
            autoClose: true,
            onClose: (secId, rowId, direction) => {
                this.setState({
                    selectedDeviceId: -1
                });
            },
            onOpen: (secId, rowId, direction) => {
                this.setState({
                    selectedDeviceId: this.props.item.id
                });
            },
            right: [
                {
                    onPress: () => {
                        // alert("Edit");    
                        this.props.showModal('update', this.props.item);            
                    },
                    text: 'Edit', type: 'primary'
                },
                {
                    onPress: this._handleDelete,
                    text: 'Delete', type: 'delete'
                }
            ],
            rowId: this.props.index,
            sectionId: 1
        };
        let backgroundColor = this.props.index % 2 == 0 ? 'darksalmon' : 'darkseagreen';
        return (
            <Swipeout {...swipeSettings}>
                <TouchableOpacity
                    onPress={this.props.onPress}>
                <View style={[styles.listItem, { backgroundColor }]}>
                    <View style={{ width: '15%', justifyContent: 'center' }}>
                        <Image source={{ uri: this.props.item.deviceURL }}
                            style={styles.deviceImage}
                        />
                    </View>
                    <View style={{ width: '85%', }} >
                        <Text style={styles.textItem}>{this.props.item.deviceName}</Text>
                        <Text numberOfLines={2} style={styles.descriptionItem}>{this.props.item.description}</Text>
                        <View style={styles.bottomLine}></View>
                    </View>
                </View>
                </TouchableOpacity>
            </Swipeout>
        );
    }
}

export default class DeviceComponent extends Component {
    static navigationOptions = {
        headerTitle: "Home",
        headerTitleStyle: {
            // fontWeight: '500',
            // fontSize: 50,
            // marginTop: 5,
            textAlign: 'center',
            alignSelf: 'center'
        },
        headerStyle: {
            backgroundColor: 'darkred',
        },
        headerTintColor: 'white',
        headerRight: (
            <Button iconLeft transparent onPress={()=>{
                showMyModal('insert', {});
            }}
            >
                <Icon name='ios-add-circle-outline' style={{color: 'white', padding: 12}}/>                
            </Button>
        ),
    };

    constructor(props) {
        super(props);
        this.state = { devices: [] };
        createAllTables();
    }
    deleteAnItem = (deviceId) => {
        deleteTblDeviceById(deviceId)
        this.queryDevice();
    }
    componentDidMount(){
        queryAllTblDevice(allDevices =>{
            this.setState({devices: allDevices});
        });
    }

    queryDevice = ()=>{
        queryAllTblDevice(allDevices =>{
            this.setState({devices: allDevices});
        });
    }

    updateAnItem = (deviceId, updatingDevice) => {
        updateTblDevice(deviceId, updatingDevice);
        this.queryDevice();
    }
    insertAnItem = (newDevice) => {
        newDevice.deviceURL = "https://bcdcog.com/wp-content/uploads/2016/05/profile-default-02.png";
        insertTblDevice(newDevice);
        this.queryDevice();
    }
    _renderItem = ({ item, index }) => {    
        //console.log(`ID=========${JSON.stringify(item)}`)           
        return (        
        <MyListItem
            // {...item}
            item={item}
            index={index}
            deleteAnItem={this.deleteAnItem}
            updateAnItem={this.updateAnItem}
            onPress={() => {
                // alert("ss");
                this.props.navigation.navigate('HistoricalComponent', {
                    histories: item.histories,
                    deviceName: item.deviceName,
                    deviceURL: item.deviceURL,
                    description: item.description
                });
            }}
            // deviceModal={this.refs.deviceModal}
            showModal={this.showModal}
        />
    )};
    showModal = (actionType, device) => {
        this.refs.deviceModal.showModal(actionType, device);
    }
    render() {
             
        showMyModal = this.showModal;   
        return (
            <SafeAreaView style={styles.container}>                
                <FlatList
                    data={this.state.devices}
                    keyExtractor={(item, index) => `${item.id}`}
                    renderItem={this._renderItem}
                    style={{ backgroundColor: 'darkseagreen' }}
                />
                <DeviceModal ref={"deviceModal"}                                                                             
                    insertAnItem={this.insertAnItem}
                    deleteAnItem={this.deleteAnItem}
                    updateAnItem={this.updateAnItem}                       
                />
            </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    listItem: {
        backgroundColor: 'white',
        flexDirection: 'row',
        paddingHorizontal: 10,
    },
    deviceImage: {
        width: 40,
        height: 40,
        borderRadius: 20,
        resizeMode: Image.resizeMode.cover,
        // resizeMode: Image.resizeMode.contain,
    },
    textItem: {
        paddingTop: 10,
        fontSize: 14,
        color: '#000000',
        fontWeight: 'bold',
        marginBottom: 5,
    },
    descriptionItem: {
        fontSize: 14,
        paddingBottom: 10
    },
    bottomLine: {
        height: 1,
        backgroundColor: 'gray'
    }
});