//
//  ViewController.swift
//  IOSApp
//
//  Created by Nguyen Duc Hoang on 5/24/18.
//  Copyright © 2018 Nguyen Duc Hoang. All rights reserved.
//

import UIKit
import React

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func btnMap(sender : UIButton) {
        let jsCodeLocation = URL(string: "http://localhost:8081/index.bundle?platform=ios")
        let mockData:NSDictionary = ["users":
            [
                ["name":"Hoang", "value":"40"],
                ["name":"Joel", "age":"10"]
            ]
        ]
        
        let rootView = RCTRootView(
            bundleURL: jsCodeLocation,
//            moduleName: "MyMapView",
            moduleName: "MyRNView",
            initialProperties: mockData as [NSObject : AnyObject],
            launchOptions: nil
        )
        let vc = UIViewController()
        vc.view = rootView        
        self.present(vc, animated: true, completion: nil)
        
    }

}

