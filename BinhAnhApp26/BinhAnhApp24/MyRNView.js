import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Animated, Easing
} from 'react-native';
export default class MyRNView extends Component {
    constructor(props) {
        super(props);        
    }
    
    componentDidMount() {
        alert(`this.props = ${JSON.stringify(this.props)}`);
    }    
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'blue',                
                justifyContent: 'center'}}>
                <Text style={{fontSize: 23, color: 'white'}}>
                    This is RN Text
                </Text>
            </View>
        );
    }
}