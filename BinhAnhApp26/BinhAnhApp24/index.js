// react-native install react-native-maps
// react-native install react-native-maps@0.16.4
// react-native install react-native-geocoder

import { AppRegistry } from 'react-native';
import App from './App';
import MyRNView from './MyRNView';

AppRegistry.registerComponent('MyMapView', () => App);
AppRegistry.registerComponent('MyRNView', () => MyRNView);
